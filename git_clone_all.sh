#!/bin/bash
#Script to get all repositories under a user from bitbucket
#Usage: getAllRepos.sh [username]
SRC_ROOT=$HOME/src
mkdir -p ${SRC_ROOT}

if [ -z "$1" ]; then
	echo 'git_clone_all.sh <bitbucket_id>' 
  exit 1
fi

./bitbucket.py $1  | \

while read repo_name
do
     #echo $repo_name
     git clone ssh://git@bitbucket.org/$1/$repo_name $SRC_ROOT/$repo_name
done
