#!/usr/bin/env python
from __future__ import print_function
import json
import sys
import requests
import getpass


'''
Given the bitbucket.org user id,
As bitbucket returns paged collection for REST API,
we need to figure out the repositories names (till the last page)
'''


def get_repo_names(url, user_id, password):

    repo_names = []
    response = requests.get(url, auth=(user_id, password))
    data = response.json()

    repos = data['values']
    for repo in repos:
        repo_names.append(repo['name'])

    # if there is a next page, get the next page till it is no more next page
    if 'next' in data:
        next_page = data['next']
        repo_names.extend(get_repo_names(next_page, user_id, password))

    return repo_names

def main():
    bb_user_id=sys.argv[1]
    bb_password=getpass.getpass('bitbucket.org password for {0}:'.format(bb_user_id))
    url='https://bitbucket.org/api/2.0/repositories/{0}'.format(bb_user_id)
    repo_names=sorted(get_repo_names(url,bb_user_id,bb_password))

    for name in repo_names:
        print(name)

if __name__ == '__main__':
    main()
